# ImagePro

#### 介绍
winform开发的图片处理工具，实现聚类提取图片主色调、滤镜、重新上色等图片处理功能。

#### 软件架构
- 使用winform开发；
- UI设计使用[DevExpress(devexpress.com)](https://www.devexpress.com/)；

#### 项目预览

![image-20210715222530454](https://gitee.com/Hook-shy/painter/raw/master/demo_img/image-20210715222530454.png)

![image-20210715222755824](https://gitee.com/Hook-shy/painter/raw/master/demo_img/image-20210715222755824.png)

![image-20210715223134657](https://gitee.com/Hook-shy/painter/raw/master/demo_img/image-20210715223134657.png)

![image-20210715223303113](https://gitee.com/Hook-shy/painter/raw/master/demo_img/image-20210715223303113.png)

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
