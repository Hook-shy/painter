﻿
namespace ImagePro
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemOpen = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.skinDropDownButtonItem1 = new DevExpress.XtraBars.SkinDropDownButtonItem();
            this.skinPaletteRibbonGalleryBarItem1 = new DevExpress.XtraBars.SkinPaletteRibbonGalleryBarItem();
            this.barButtonItemHistogram = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemClassCount = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.barEditItemLimit = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.barEditItemSkip = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemSpinEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.barButtonItemStartClasses = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonGalleryBarItem1 = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenuClasses = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemObject = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemColoring = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenuCustom = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemGray = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAnti = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemTLR = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemTUD = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemVal2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemResize = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemColorPickEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemColorPickEdit();
            this.barButtonItemFilter = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenuFilter = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barStaticItem = new DevExpress.XtraBars.BarStaticItem();
            this.barButtonItemRename = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCopyImg = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDel = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.galleryDropDownFilter = new DevExpress.XtraBars.Ribbon.GalleryDropDown(this.components);
            this.ribbonGalleryBarItemFilter = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.barButtonItemTC = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem13 = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenuRotation = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemRotationShun = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRotationNi = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageCategory1 = new DevExpress.XtraBars.Ribbon.RibbonPageCategory();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupFile = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupObj = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupImg = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupFilter = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupClasses = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupPaint = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupExchange = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage3 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage4 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemColorPickEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemColorPickEdit();
            this.repositoryItemColorEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.repositoryItemZoomTrackBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar();
            this.repositoryItemProgressBar2 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.repositoryItemColorPickEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemColorPickEdit();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemRibbonSearchEdit1 = new DevExpress.XtraBars.Ribbon.Internal.RepositoryItemRibbonSearchEdit();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.dockManager = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel2 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel2_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.documentManager = new DevExpress.XtraBars.Docking2010.DocumentManager(this.components);
            this.tabbedView1 = new DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView(this.components);
            this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::ImagePro.FrmLoading), true, true);
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemToggleSwitch1 = new DevExpress.XtraEditors.Repository.RepositoryItemToggleSwitch();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuClasses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuCustom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorPickEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.galleryDropDownFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuRotation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorPickEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorPickEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRibbonSearchEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).BeginInit();
            this.dockPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemToggleSwitch1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.ribbon.SearchEditItem,
            this.barButtonItemOpen,
            this.barButtonItemSave,
            this.skinDropDownButtonItem1,
            this.skinPaletteRibbonGalleryBarItem1,
            this.barButtonItemHistogram,
            this.barEditItemClassCount,
            this.barEditItemLimit,
            this.barEditItemSkip,
            this.barButtonItemStartClasses,
            this.ribbonGalleryBarItem1,
            this.barButtonItem6,
            this.barButtonItemObject,
            this.barButtonItemColoring,
            this.barButtonItem5,
            this.barButtonItemGray,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItemAnti,
            this.barButtonItemTLR,
            this.barButtonItemTUD,
            this.barButtonItemVal2,
            this.barButtonItemResize,
            this.barEditItem1,
            this.barButtonItemFilter,
            this.barStaticItem,
            this.barButtonItemRename,
            this.barButtonItemCopyImg,
            this.barButtonItemDel,
            this.barButtonItem10,
            this.ribbonGalleryBarItemFilter,
            this.barButtonItemTC,
            this.barButtonItem13,
            this.barButtonItemRotationShun,
            this.barButtonItemRotationNi,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem8,
            this.barButtonItem9});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 64;
            this.ribbon.Name = "ribbon";
            this.ribbon.PageCategories.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageCategory[] {
            this.ribbonPageCategory1});
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1,
            this.ribbonPage2,
            this.ribbonPage3,
            this.ribbonPage4});
            this.ribbon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemSpinEdit2,
            this.repositoryItemSpinEdit3,
            this.repositoryItemColorPickEdit1,
            this.repositoryItemColorEdit1,
            this.repositoryItemPictureEdit1,
            this.repositoryItemComboBox1,
            this.repositoryItemColorPickEdit2,
            this.repositoryItemProgressBar1,
            this.repositoryItemZoomTrackBar1,
            this.repositoryItemProgressBar2,
            this.repositoryItemColorPickEdit3,
            this.repositoryItemButtonEdit1,
            this.repositoryItemRibbonSearchEdit1,
            this.repositoryItemToggleSwitch1});
            this.ribbon.Size = new System.Drawing.Size(1269, 162);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            // 
            // barButtonItemOpen
            // 
            this.barButtonItemOpen.Caption = "打开图片";
            this.barButtonItemOpen.Id = 1;
            this.barButtonItemOpen.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemOpen.ImageOptions.SvgImage")));
            this.barButtonItemOpen.Name = "barButtonItemOpen";
            this.barButtonItemOpen.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemOpen_ItemClick);
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Caption = "保存图片";
            this.barButtonItemSave.Id = 2;
            this.barButtonItemSave.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemSave.ImageOptions.SvgImage")));
            this.barButtonItemSave.Name = "barButtonItemSave";
            this.barButtonItemSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSave_ItemClick);
            // 
            // skinDropDownButtonItem1
            // 
            this.skinDropDownButtonItem1.Id = 3;
            this.skinDropDownButtonItem1.Name = "skinDropDownButtonItem1";
            // 
            // skinPaletteRibbonGalleryBarItem1
            // 
            this.skinPaletteRibbonGalleryBarItem1.Caption = "skinPaletteRibbonGalleryBarItem1";
            this.skinPaletteRibbonGalleryBarItem1.Id = 4;
            this.skinPaletteRibbonGalleryBarItem1.Name = "skinPaletteRibbonGalleryBarItem1";
            // 
            // barButtonItemHistogram
            // 
            this.barButtonItemHistogram.Caption = "直方图";
            this.barButtonItemHistogram.Id = 5;
            this.barButtonItemHistogram.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemHistogram.ImageOptions.SvgImage")));
            this.barButtonItemHistogram.Name = "barButtonItemHistogram";
            this.barButtonItemHistogram.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemHistogram_ItemClick);
            // 
            // barEditItemClassCount
            // 
            this.barEditItemClassCount.Caption = "聚类数量：";
            this.barEditItemClassCount.Edit = this.repositoryItemSpinEdit1;
            this.barEditItemClassCount.EditValue = 2;
            this.barEditItemClassCount.Id = 7;
            this.barEditItemClassCount.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barEditItemClassCount.ImageOptions.SvgImage")));
            this.barEditItemClassCount.Name = "barEditItemClassCount";
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // barEditItemLimit
            // 
            this.barEditItemLimit.Caption = "迭代限制：";
            this.barEditItemLimit.Edit = this.repositoryItemSpinEdit2;
            this.barEditItemLimit.EditValue = 100;
            this.barEditItemLimit.Id = 8;
            this.barEditItemLimit.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barEditItemLimit.ImageOptions.SvgImage")));
            this.barEditItemLimit.Name = "barEditItemLimit";
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit2.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.MinValue = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            // 
            // barEditItemSkip
            // 
            this.barEditItemSkip.Caption = "取色间隔：";
            this.barEditItemSkip.Edit = this.repositoryItemSpinEdit3;
            this.barEditItemSkip.EditValue = 10;
            this.barEditItemSkip.Id = 9;
            this.barEditItemSkip.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barEditItemSkip.ImageOptions.SvgImage")));
            this.barEditItemSkip.Name = "barEditItemSkip";
            // 
            // repositoryItemSpinEdit3
            // 
            this.repositoryItemSpinEdit3.AutoHeight = false;
            this.repositoryItemSpinEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit3.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.repositoryItemSpinEdit3.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemSpinEdit3.Name = "repositoryItemSpinEdit3";
            // 
            // barButtonItemStartClasses
            // 
            this.barButtonItemStartClasses.Caption = "开始提取";
            this.barButtonItemStartClasses.Id = 10;
            this.barButtonItemStartClasses.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemStartClasses.ImageOptions.SvgImage")));
            this.barButtonItemStartClasses.Name = "barButtonItemStartClasses";
            this.barButtonItemStartClasses.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemStartClasses_ItemClick);
            // 
            // ribbonGalleryBarItem1
            // 
            this.ribbonGalleryBarItem1.Caption = "ribbonGalleryBarItem1";
            this.ribbonGalleryBarItem1.Id = 13;
            this.ribbonGalleryBarItem1.Name = "ribbonGalleryBarItem1";
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.ActAsDropDown = true;
            this.barButtonItem6.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItem6.Caption = "聚类着色";
            this.barButtonItem6.DropDownControl = this.popupMenuClasses;
            this.barButtonItem6.Id = 16;
            this.barButtonItem6.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItem6.ImageOptions.SvgImage")));
            this.barButtonItem6.Name = "barButtonItem6";
            // 
            // popupMenuClasses
            // 
            this.popupMenuClasses.Name = "popupMenuClasses";
            this.popupMenuClasses.Ribbon = this.ribbon;
            // 
            // barButtonItemObject
            // 
            this.barButtonItemObject.Caption = "对象浏览器";
            this.barButtonItemObject.Id = 23;
            this.barButtonItemObject.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemObject.ImageOptions.SvgImage")));
            this.barButtonItemObject.Name = "barButtonItemObject";
            this.barButtonItemObject.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemObject_ItemClick);
            // 
            // barButtonItemColoring
            // 
            this.barButtonItemColoring.Caption = "着色选项";
            this.barButtonItemColoring.Id = 24;
            this.barButtonItemColoring.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemColoring.ImageOptions.SvgImage")));
            this.barButtonItemColoring.Name = "barButtonItemColoring";
            this.barButtonItemColoring.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemColoring_ItemClick);
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.ActAsDropDown = true;
            this.barButtonItem5.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItem5.Caption = "自定义着色";
            this.barButtonItem5.DropDownControl = this.popupMenuCustom;
            this.barButtonItem5.Id = 25;
            this.barButtonItem5.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItem5.ImageOptions.SvgImage")));
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // popupMenuCustom
            // 
            this.popupMenuCustom.Name = "popupMenuCustom";
            this.popupMenuCustom.Ribbon = this.ribbon;
            // 
            // barButtonItemGray
            // 
            this.barButtonItemGray.Caption = "灰度处理";
            this.barButtonItemGray.Id = 27;
            this.barButtonItemGray.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemGray.ImageOptions.SvgImage")));
            this.barButtonItemGray.Name = "barButtonItemGray";
            this.barButtonItemGray.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemGray_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "barButtonItem3";
            this.barButtonItem3.Id = 28;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "barButtonItem4";
            this.barButtonItem4.Id = 29;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItemAnti
            // 
            this.barButtonItemAnti.Caption = "反色处理";
            this.barButtonItemAnti.Id = 30;
            this.barButtonItemAnti.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemAnti.ImageOptions.SvgImage")));
            this.barButtonItemAnti.Name = "barButtonItemAnti";
            this.barButtonItemAnti.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemAnti_ItemClick);
            // 
            // barButtonItemTLR
            // 
            this.barButtonItemTLR.Caption = "左右镜像";
            this.barButtonItemTLR.Id = 31;
            this.barButtonItemTLR.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemTLR.ImageOptions.SvgImage")));
            this.barButtonItemTLR.Name = "barButtonItemTLR";
            this.barButtonItemTLR.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)((DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.barButtonItemTLR.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemTLR_ItemClick);
            // 
            // barButtonItemTUD
            // 
            this.barButtonItemTUD.Caption = "上下镜像";
            this.barButtonItemTUD.Id = 32;
            this.barButtonItemTUD.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemTUD.ImageOptions.SvgImage")));
            this.barButtonItemTUD.Name = "barButtonItemTUD";
            this.barButtonItemTUD.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)((DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.barButtonItemTUD.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemTUD_ItemClick);
            // 
            // barButtonItemVal2
            // 
            this.barButtonItemVal2.Caption = "二值处理";
            this.barButtonItemVal2.Id = 35;
            this.barButtonItemVal2.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemVal2.ImageOptions.SvgImage")));
            this.barButtonItemVal2.Name = "barButtonItemVal2";
            this.barButtonItemVal2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemVal2_ItemClick);
            // 
            // barButtonItemResize
            // 
            this.barButtonItemResize.Caption = "尺寸调节";
            this.barButtonItemResize.Id = 37;
            this.barButtonItemResize.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemResize.ImageOptions.SvgImage")));
            this.barButtonItemResize.Name = "barButtonItemResize";
            this.barButtonItemResize.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemResize_ItemClick);
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Edit = this.repositoryItemColorPickEdit2;
            this.barEditItem1.Id = 38;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemColorPickEdit2
            // 
            this.repositoryItemColorPickEdit2.AutoHeight = false;
            this.repositoryItemColorPickEdit2.AutomaticColor = System.Drawing.Color.Black;
            this.repositoryItemColorPickEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorPickEdit2.Name = "repositoryItemColorPickEdit2";
            // 
            // barButtonItemFilter
            // 
            this.barButtonItemFilter.ActAsDropDown = true;
            this.barButtonItemFilter.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItemFilter.Caption = "滤镜";
            this.barButtonItemFilter.DropDownControl = this.popupMenuFilter;
            this.barButtonItemFilter.Id = 39;
            this.barButtonItemFilter.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemFilter.ImageOptions.SvgImage")));
            this.barButtonItemFilter.Name = "barButtonItemFilter";
            this.barButtonItemFilter.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // popupMenuFilter
            // 
            this.popupMenuFilter.Name = "popupMenuFilter";
            this.popupMenuFilter.Ribbon = this.ribbon;
            // 
            // barStaticItem
            // 
            this.barStaticItem.Caption = "就绪";
            this.barStaticItem.Id = 40;
            this.barStaticItem.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barStaticItem.ImageOptions.SvgImage")));
            this.barStaticItem.Name = "barStaticItem";
            this.barStaticItem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItemRename
            // 
            this.barButtonItemRename.Caption = "重命名";
            this.barButtonItemRename.Id = 44;
            this.barButtonItemRename.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemRename.ImageOptions.SvgImage")));
            this.barButtonItemRename.Name = "barButtonItemRename";
            this.barButtonItemRename.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)((DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.barButtonItemRename.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemRename_ItemClick);
            // 
            // barButtonItemCopyImg
            // 
            this.barButtonItemCopyImg.Caption = "复制图像";
            this.barButtonItemCopyImg.Id = 45;
            this.barButtonItemCopyImg.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemCopyImg.ImageOptions.SvgImage")));
            this.barButtonItemCopyImg.Name = "barButtonItemCopyImg";
            this.barButtonItemCopyImg.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCopyImg_ItemClick);
            // 
            // barButtonItemDel
            // 
            this.barButtonItemDel.Caption = "销毁";
            this.barButtonItemDel.Id = 47;
            this.barButtonItemDel.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemDel.ImageOptions.SvgImage")));
            this.barButtonItemDel.Name = "barButtonItemDel";
            this.barButtonItemDel.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)((DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.barButtonItemDel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemDel_ItemClick);
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.ActAsDropDown = true;
            this.barButtonItem10.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItem10.Caption = "预置滤镜";
            this.barButtonItem10.DropDownControl = this.galleryDropDownFilter;
            this.barButtonItem10.Id = 48;
            this.barButtonItem10.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItem10.ImageOptions.SvgImage")));
            this.barButtonItem10.Name = "barButtonItem10";
            // 
            // galleryDropDownFilter
            // 
            this.galleryDropDownFilter.Name = "galleryDropDownFilter";
            this.galleryDropDownFilter.Ribbon = this.ribbon;
            // 
            // ribbonGalleryBarItemFilter
            // 
            this.ribbonGalleryBarItemFilter.Caption = "ribbonGalleryBarItem2";
            // 
            // 
            // 
            this.ribbonGalleryBarItemFilter.Gallery.ImageSize = new System.Drawing.Size(32, 32);
            this.ribbonGalleryBarItemFilter.Id = 49;
            this.ribbonGalleryBarItemFilter.Name = "ribbonGalleryBarItemFilter";
            // 
            // barButtonItemTC
            // 
            this.barButtonItemTC.Caption = "中心镜像";
            this.barButtonItemTC.Id = 52;
            this.barButtonItemTC.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemTC.ImageOptions.SvgImage")));
            this.barButtonItemTC.Name = "barButtonItemTC";
            this.barButtonItemTC.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)((DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.barButtonItemTC.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemTC_ItemClick);
            // 
            // barButtonItem13
            // 
            this.barButtonItem13.ActAsDropDown = true;
            this.barButtonItem13.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItem13.Caption = "旋转图像";
            this.barButtonItem13.DropDownControl = this.popupMenuRotation;
            this.barButtonItem13.Id = 53;
            this.barButtonItem13.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItem13.ImageOptions.SvgImage")));
            this.barButtonItem13.Name = "barButtonItem13";
            // 
            // popupMenuRotation
            // 
            this.popupMenuRotation.ItemLinks.Add(this.barButtonItemRotationShun);
            this.popupMenuRotation.ItemLinks.Add(this.barButtonItemRotationNi);
            this.popupMenuRotation.Name = "popupMenuRotation";
            this.popupMenuRotation.Ribbon = this.ribbon;
            // 
            // barButtonItemRotationShun
            // 
            this.barButtonItemRotationShun.Caption = "顺时针旋转";
            this.barButtonItemRotationShun.Id = 54;
            this.barButtonItemRotationShun.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemRotationShun.ImageOptions.SvgImage")));
            this.barButtonItemRotationShun.Name = "barButtonItemRotationShun";
            this.barButtonItemRotationShun.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemRotationShun_ItemClick);
            // 
            // barButtonItemRotationNi
            // 
            this.barButtonItemRotationNi.Caption = "逆时针旋转";
            this.barButtonItemRotationNi.Id = 55;
            this.barButtonItemRotationNi.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemRotationNi.ImageOptions.SvgImage")));
            this.barButtonItemRotationNi.Name = "barButtonItemRotationNi";
            this.barButtonItemRotationNi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemRotationNi_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "使用向导";
            this.barButtonItem1.Id = 57;
            this.barButtonItem1.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItem1.ImageOptions.SvgImage")));
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "关于";
            this.barButtonItem2.Id = 58;
            this.barButtonItem2.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItem2.ImageOptions.SvgImage")));
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // ribbonPageCategory1
            // 
            this.ribbonPageCategory1.Name = "ribbonPageCategory1";
            this.ribbonPageCategory1.Text = "ribbonPageCategory1";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupFile,
            this.ribbonPageGroupObj,
            this.ribbonPageGroup1,
            this.ribbonPageGroup2});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "开始";
            // 
            // ribbonPageGroupFile
            // 
            this.ribbonPageGroupFile.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ribbonPageGroupFile.ImageOptions.SvgImage")));
            this.ribbonPageGroupFile.ItemLinks.Add(this.barButtonItemOpen);
            this.ribbonPageGroupFile.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupFile.Name = "ribbonPageGroupFile";
            this.ribbonPageGroupFile.Text = "文件";
            // 
            // ribbonPageGroupObj
            // 
            this.ribbonPageGroupObj.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ribbonPageGroupObj.ImageOptions.SvgImage")));
            this.ribbonPageGroupObj.ItemLinks.Add(this.barButtonItemCopyImg);
            this.ribbonPageGroupObj.ItemLinks.Add(this.barButtonItemRename);
            this.ribbonPageGroupObj.ItemLinks.Add(this.barButtonItemDel);
            this.ribbonPageGroupObj.Name = "ribbonPageGroupObj";
            this.ribbonPageGroupObj.Text = "对象";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "帮助";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem2);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "关于";
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupImg,
            this.ribbonPageGroupFilter,
            this.ribbonPageGroupClasses,
            this.ribbonPageGroupPaint,
            this.ribbonPageGroupExchange});
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "操作";
            // 
            // ribbonPageGroupImg
            // 
            this.ribbonPageGroupImg.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ribbonPageGroupImg.ImageOptions.SvgImage")));
            this.ribbonPageGroupImg.ItemLinks.Add(this.barButtonItemHistogram);
            this.ribbonPageGroupImg.ItemLinks.Add(this.barButtonItemGray);
            this.ribbonPageGroupImg.ItemLinks.Add(this.barButtonItemAnti);
            this.ribbonPageGroupImg.ItemLinks.Add(this.barButtonItemVal2);
            this.ribbonPageGroupImg.Name = "ribbonPageGroupImg";
            this.ribbonPageGroupImg.Text = "图像";
            // 
            // ribbonPageGroupFilter
            // 
            this.ribbonPageGroupFilter.ItemLinks.Add(this.barButtonItemFilter);
            this.ribbonPageGroupFilter.ItemLinks.Add(this.ribbonGalleryBarItemFilter);
            this.ribbonPageGroupFilter.ItemLinks.Add(this.barButtonItem10);
            this.ribbonPageGroupFilter.Name = "ribbonPageGroupFilter";
            this.ribbonPageGroupFilter.Text = "滤镜";
            // 
            // ribbonPageGroupClasses
            // 
            this.ribbonPageGroupClasses.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ribbonPageGroupClasses.ImageOptions.SvgImage")));
            this.ribbonPageGroupClasses.ItemLinks.Add(this.barEditItemClassCount);
            this.ribbonPageGroupClasses.ItemLinks.Add(this.barEditItemLimit);
            this.ribbonPageGroupClasses.ItemLinks.Add(this.barEditItemSkip);
            this.ribbonPageGroupClasses.ItemLinks.Add(this.barButtonItemStartClasses);
            this.ribbonPageGroupClasses.Name = "ribbonPageGroupClasses";
            this.ribbonPageGroupClasses.Text = "聚类提取主色调";
            // 
            // ribbonPageGroupPaint
            // 
            this.ribbonPageGroupPaint.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ribbonPageGroupPaint.ImageOptions.SvgImage")));
            this.ribbonPageGroupPaint.ItemLinks.Add(this.barButtonItem6);
            this.ribbonPageGroupPaint.ItemLinks.Add(this.barButtonItem5);
            this.ribbonPageGroupPaint.Name = "ribbonPageGroupPaint";
            this.ribbonPageGroupPaint.Text = "重新着色";
            // 
            // ribbonPageGroupExchange
            // 
            this.ribbonPageGroupExchange.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ribbonPageGroupExchange.ImageOptions.SvgImage")));
            this.ribbonPageGroupExchange.ItemLinks.Add(this.barButtonItem13);
            this.ribbonPageGroupExchange.ItemLinks.Add(this.barButtonItemTLR);
            this.ribbonPageGroupExchange.ItemLinks.Add(this.barButtonItemTUD);
            this.ribbonPageGroupExchange.ItemLinks.Add(this.barButtonItemTC);
            this.ribbonPageGroupExchange.ItemLinks.Add(this.barButtonItemResize);
            this.ribbonPageGroupExchange.Name = "ribbonPageGroupExchange";
            this.ribbonPageGroupExchange.Text = "变换";
            // 
            // ribbonPage3
            // 
            this.ribbonPage3.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup3});
            this.ribbonPage3.Name = "ribbonPage3";
            this.ribbonPage3.Text = "视图";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItemObject);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItemColoring);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "视图";
            // 
            // ribbonPage4
            // 
            this.ribbonPage4.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup4});
            this.ribbonPage4.Name = "ribbonPage4";
            this.ribbonPage4.Text = "主题";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.skinDropDownButtonItem1);
            this.ribbonPageGroup4.ItemLinks.Add(this.skinPaletteRibbonGalleryBarItem1);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "ribbonPageGroup4";
            // 
            // repositoryItemColorPickEdit1
            // 
            this.repositoryItemColorPickEdit1.AutoHeight = false;
            this.repositoryItemColorPickEdit1.AutomaticColor = System.Drawing.Color.Black;
            this.repositoryItemColorPickEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorPickEdit1.Name = "repositoryItemColorPickEdit1";
            this.repositoryItemColorPickEdit1.ShowWebSafeColors = true;
            // 
            // repositoryItemColorEdit1
            // 
            this.repositoryItemColorEdit1.AutoHeight = false;
            this.repositoryItemColorEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit1.Name = "repositoryItemColorEdit1";
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            // 
            // repositoryItemZoomTrackBar1
            // 
            this.repositoryItemZoomTrackBar1.Name = "repositoryItemZoomTrackBar1";
            // 
            // repositoryItemProgressBar2
            // 
            this.repositoryItemProgressBar2.Name = "repositoryItemProgressBar2";
            // 
            // repositoryItemColorPickEdit3
            // 
            this.repositoryItemColorPickEdit3.AutoHeight = false;
            this.repositoryItemColorPickEdit3.AutomaticColor = System.Drawing.Color.Black;
            this.repositoryItemColorPickEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorPickEdit3.Name = "repositoryItemColorPickEdit3";
            this.repositoryItemColorPickEdit3.ShowWebSafeColors = true;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            // 
            // repositoryItemRibbonSearchEdit1
            // 
            this.repositoryItemRibbonSearchEdit1.AutoHeight = false;
            this.repositoryItemRibbonSearchEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemRibbonSearchEdit1.Name = "repositoryItemRibbonSearchEdit1";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.barStaticItem);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 668);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1269, 22);
            // 
            // dockManager
            // 
            this.dockManager.Form = this;
            this.dockManager.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel2});
            this.dockManager.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane",
            "DevExpress.XtraBars.TabFormControl",
            "DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl",
            "DevExpress.XtraBars.ToolbarForm.ToolbarFormControl"});
            // 
            // dockPanel2
            // 
            this.dockPanel2.Controls.Add(this.dockPanel2_Container);
            this.dockPanel2.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanel2.ID = new System.Guid("5b24d7f5-75b4-49f6-868e-09f264c72394");
            this.dockPanel2.Location = new System.Drawing.Point(1069, 162);
            this.dockPanel2.Name = "dockPanel2";
            this.dockPanel2.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanel2.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanel2.SavedIndex = 1;
            this.dockPanel2.Size = new System.Drawing.Size(200, 506);
            this.dockPanel2.Text = "dockPanel2";
            this.dockPanel2.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            // 
            // dockPanel2_Container
            // 
            this.dockPanel2_Container.Location = new System.Drawing.Point(4, 46);
            this.dockPanel2_Container.Name = "dockPanel2_Container";
            this.dockPanel2_Container.Size = new System.Drawing.Size(193, 457);
            this.dockPanel2_Container.TabIndex = 0;
            // 
            // documentManager
            // 
            this.documentManager.MdiParent = this;
            this.documentManager.MenuManager = this.ribbon;
            this.documentManager.View = this.tabbedView1;
            this.documentManager.ViewCollection.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseView[] {
            this.tabbedView1});
            // 
            // splashScreenManager
            // 
            this.splashScreenManager.ClosingDelay = 500;
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "单色";
            this.barButtonItem8.Id = 60;
            this.barButtonItem8.Name = "barButtonItem8";
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "原色";
            this.barButtonItem9.Id = 61;
            this.barButtonItem9.Name = "barButtonItem9";
            // 
            // repositoryItemToggleSwitch1
            // 
            this.repositoryItemToggleSwitch1.AutoHeight = false;
            this.repositoryItemToggleSwitch1.Name = "repositoryItemToggleSwitch1";
            this.repositoryItemToggleSwitch1.OffText = "Off";
            this.repositoryItemToggleSwitch1.OnText = "On";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1269, 690);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.IconOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("FrmMain.IconOptions.SvgImage")));
            this.IsMdiContainer = true;
            this.Name = "FrmMain";
            this.Ribbon = this.ribbon;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "Painter";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuClasses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuCustom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorPickEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.galleryDropDownFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuRotation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorPickEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorPickEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRibbonSearchEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).EndInit();
            this.dockPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.documentManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemToggleSwitch1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupFile;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.Docking.DockManager dockManager;
        private DevExpress.XtraBars.Docking2010.DocumentManager documentManager;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView tabbedView1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemOpen;
        private DevExpress.XtraBars.BarButtonItem barButtonItemSave;
        private DevExpress.XtraBars.SkinDropDownButtonItem skinDropDownButtonItem1;
        private DevExpress.XtraBars.SkinPaletteRibbonGalleryBarItem skinPaletteRibbonGalleryBarItem1;
        private DevExpress.XtraBars.Ribbon.RibbonPageCategory ribbonPageCategory1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupImg;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel2;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel2_Container;
        private DevExpress.XtraBars.BarButtonItem barButtonItemHistogram;
        private DevExpress.XtraBars.BarEditItem barEditItemClassCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraBars.BarEditItem barEditItemLimit;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraBars.BarEditItem barEditItemSkip;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit3;
        private DevExpress.XtraBars.BarButtonItem barButtonItemStartClasses;
        private DevExpress.XtraBars.RibbonGalleryBarItem ribbonGalleryBarItem1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupClasses;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupPaint;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorPickEdit repositoryItemColorPickEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemObject;
        private DevExpress.XtraBars.BarButtonItem barButtonItemColoring;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.PopupMenu popupMenuClasses;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemGray;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.PopupMenu popupMenuCustom;
        private DevExpress.XtraBars.BarButtonItem barButtonItemAnti;
        private DevExpress.XtraBars.BarButtonItem barButtonItemTLR;
        private DevExpress.XtraBars.BarButtonItem barButtonItemTUD;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupExchange;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager;
        private DevExpress.XtraBars.BarButtonItem barButtonItemVal2;
        private DevExpress.XtraBars.BarButtonItem barButtonItemResize;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorPickEdit repositoryItemColorPickEdit2;
        private DevExpress.XtraBars.BarButtonItem barButtonItemFilter;
        private DevExpress.XtraBars.BarStaticItem barStaticItem;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar2;
        private DevExpress.XtraBars.BarButtonItem barButtonItemRename;
        private DevExpress.XtraBars.BarButtonItem barButtonItemCopyImg;
        private DevExpress.XtraBars.BarButtonItem barButtonItemDel;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupObj;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar repositoryItemZoomTrackBar1;
        private DevExpress.XtraBars.PopupMenu popupMenuFilter;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraBars.Ribbon.GalleryDropDown galleryDropDownFilter;
        private DevExpress.XtraBars.RibbonGalleryBarItem ribbonGalleryBarItemFilter;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorPickEdit repositoryItemColorPickEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemTC;
        private DevExpress.XtraBars.BarButtonItem barButtonItem13;
        private DevExpress.XtraBars.PopupMenu popupMenuRotation;
        private DevExpress.XtraBars.BarButtonItem barButtonItemRotationShun;
        private DevExpress.XtraBars.BarButtonItem barButtonItemRotationNi;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.Internal.RepositoryItemRibbonSearchEdit repositoryItemRibbonSearchEdit1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraEditors.Repository.RepositoryItemToggleSwitch repositoryItemToggleSwitch1;
    }
}