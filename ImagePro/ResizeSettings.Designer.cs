﻿
namespace ImagePro
{
    partial class ResizeSettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ResizeSettings));
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.tablePanel1 = new DevExpress.Utils.Layout.TablePanel();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.spinEditWidth = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditHeight = new DevExpress.XtraEditors.SpinEdit();
            this.toggleSwitchPerCent = new DevExpress.XtraEditors.ToggleSwitch();
            this.radioGroupMode = new DevExpress.XtraEditors.RadioGroup();
            ((System.ComponentModel.ISupportInitialize)(this.tablePanel1)).BeginInit();
            this.tablePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditWidth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditHeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSwitchPerCent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupMode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // windowsUIButtonPanel1
            // 
            windowsUIButtonImageOptions2.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("windowsUIButtonImageOptions2.SvgImage")));
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("开始调整", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, null, -1, false)});
            this.windowsUIButtonPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(0, 268);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(551, 89);
            this.windowsUIButtonPanel1.TabIndex = 0;
            // 
            // tablePanel1
            // 
            this.tablePanel1.Columns.AddRange(new DevExpress.Utils.Layout.TablePanelColumn[] {
            new DevExpress.Utils.Layout.TablePanelColumn(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 10F),
            new DevExpress.Utils.Layout.TablePanelColumn(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 100F),
            new DevExpress.Utils.Layout.TablePanelColumn(DevExpress.Utils.Layout.TablePanelEntityStyle.AutoSize, 55F)});
            this.tablePanel1.Controls.Add(this.radioGroupMode);
            this.tablePanel1.Controls.Add(this.toggleSwitchPerCent);
            this.tablePanel1.Controls.Add(this.spinEditHeight);
            this.tablePanel1.Controls.Add(this.spinEditWidth);
            this.tablePanel1.Controls.Add(this.labelControl4);
            this.tablePanel1.Controls.Add(this.labelControl3);
            this.tablePanel1.Controls.Add(this.labelControl2);
            this.tablePanel1.Controls.Add(this.labelControl1);
            this.tablePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanel1.Location = new System.Drawing.Point(0, 0);
            this.tablePanel1.Name = "tablePanel1";
            this.tablePanel1.Rows.AddRange(new DevExpress.Utils.Layout.TablePanelRow[] {
            new DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.AutoSize, 26F),
            new DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.AutoSize, 26F),
            new DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.AutoSize, 26F),
            new DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.AutoSize, 26F),
            new DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 26F)});
            this.tablePanel1.Size = new System.Drawing.Size(551, 268);
            this.tablePanel1.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.tablePanel1.SetColumn(this.labelControl1, 1);
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl1.Location = new System.Drawing.Point(13, 3);
            this.labelControl1.Name = "labelControl1";
            this.tablePanel1.SetRow(this.labelControl1, 0);
            this.labelControl1.Size = new System.Drawing.Size(94, 20);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "宽度(px)：";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.tablePanel1.SetColumn(this.labelControl2, 1);
            this.labelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl2.Location = new System.Drawing.Point(13, 29);
            this.labelControl2.Name = "labelControl2";
            this.tablePanel1.SetRow(this.labelControl2, 1);
            this.labelControl2.Size = new System.Drawing.Size(94, 20);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "高度(px)：";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.tablePanel1.SetColumn(this.labelControl3, 1);
            this.labelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl3.Location = new System.Drawing.Point(13, 55);
            this.labelControl3.Name = "labelControl3";
            this.tablePanel1.SetRow(this.labelControl3, 2);
            this.labelControl3.Size = new System.Drawing.Size(94, 19);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "使用百分比：";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.tablePanel1.SetColumn(this.labelControl4, 1);
            this.labelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl4.Location = new System.Drawing.Point(13, 80);
            this.labelControl4.Name = "labelControl4";
            this.tablePanel1.SetRow(this.labelControl4, 3);
            this.labelControl4.Size = new System.Drawing.Size(94, 96);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "填充模式：";
            // 
            // spinEditWidth
            // 
            this.tablePanel1.SetColumn(this.spinEditWidth, 2);
            this.spinEditWidth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spinEditWidth.EditValue = new decimal(new int[] {
            1600,
            0,
            0,
            0});
            this.spinEditWidth.Location = new System.Drawing.Point(113, 3);
            this.spinEditWidth.Name = "spinEditWidth";
            this.spinEditWidth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditWidth.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.spinEditWidth.Properties.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.tablePanel1.SetRow(this.spinEditWidth, 0);
            this.spinEditWidth.Size = new System.Drawing.Size(435, 20);
            this.spinEditWidth.TabIndex = 4;
            // 
            // spinEditHeight
            // 
            this.tablePanel1.SetColumn(this.spinEditHeight, 2);
            this.spinEditHeight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spinEditHeight.EditValue = new decimal(new int[] {
            900,
            0,
            0,
            0});
            this.spinEditHeight.Location = new System.Drawing.Point(113, 29);
            this.spinEditHeight.Name = "spinEditHeight";
            this.spinEditHeight.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditHeight.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.spinEditHeight.Properties.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.tablePanel1.SetRow(this.spinEditHeight, 1);
            this.spinEditHeight.Size = new System.Drawing.Size(435, 20);
            this.spinEditHeight.TabIndex = 5;
            // 
            // toggleSwitchPerCent
            // 
            this.tablePanel1.SetColumn(this.toggleSwitchPerCent, 2);
            this.toggleSwitchPerCent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toggleSwitchPerCent.Location = new System.Drawing.Point(113, 55);
            this.toggleSwitchPerCent.Name = "toggleSwitchPerCent";
            this.toggleSwitchPerCent.Properties.OffText = "px";
            this.toggleSwitchPerCent.Properties.OnText = "%";
            this.tablePanel1.SetRow(this.toggleSwitchPerCent, 2);
            this.toggleSwitchPerCent.Size = new System.Drawing.Size(435, 19);
            this.toggleSwitchPerCent.TabIndex = 6;
            // 
            // radioGroupMode
            // 
            this.tablePanel1.SetColumn(this.radioGroupMode, 2);
            this.radioGroupMode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radioGroupMode.EditValue = "Fill";
            this.radioGroupMode.Location = new System.Drawing.Point(113, 80);
            this.radioGroupMode.Name = "radioGroupMode";
            this.radioGroupMode.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Fill", "Fill"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Contain", "Contain"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Cover", "Cover")});
            this.tablePanel1.SetRow(this.radioGroupMode, 3);
            this.radioGroupMode.Size = new System.Drawing.Size(435, 96);
            this.radioGroupMode.TabIndex = 7;
            // 
            // ResizeSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tablePanel1);
            this.Controls.Add(this.windowsUIButtonPanel1);
            this.Name = "ResizeSettings";
            this.Size = new System.Drawing.Size(551, 357);
            ((System.ComponentModel.ISupportInitialize)(this.tablePanel1)).EndInit();
            this.tablePanel1.ResumeLayout(false);
            this.tablePanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditWidth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditHeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSwitchPerCent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupMode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.Utils.Layout.TablePanel tablePanel1;
        private DevExpress.XtraEditors.ToggleSwitch toggleSwitchPerCent;
        private DevExpress.XtraEditors.SpinEdit spinEditHeight;
        private DevExpress.XtraEditors.SpinEdit spinEditWidth;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.RadioGroup radioGroupMode;
    }
}
