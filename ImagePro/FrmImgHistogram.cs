﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImagePro
{
    public partial class FrmImgHistogram : DevExpress.XtraEditors.XtraForm
    {
        public FrmImgHistogram(ImageItem imageItem, ColorClustering.Progress pro)
        {
            InitializeComponent();
            ImageItem = imageItem;
            Text = $"{imageItem.Name}_直方图";
            Compute(pro);
        }

        public ImageItem ImageItem { get; set; }

        private void Compute(ColorClustering.Progress pro)
        {
            int[] r = new int[256];
            int[] g = new int[256];
            int[] b = new int[256];
            for (int i = 0; i < 256; i++)
            {
                r[i] = 0;
                g[i] = 0;
                b[i] = 0;
            }
            pro(0);
            for (int i = 0; i < ImageItem.Image.Height; i++)
            {
                pro(100 * i / ImageItem.Image.Height);
                for (int j = 0; j < ImageItem.Image.Width; j++)
                {
                    Color color = ((Bitmap)ImageItem.Image).GetPixel(j, i);
                    r[color.R]++;
                    g[color.G]++;
                    b[color.B]++;
                }
            }
            pro(100);
            for (int i = 0; i < 256; i++)
            {
                chartControl1.Series[0].Points.Add(new DevExpress.XtraCharts.SeriesPoint(i + 1, r[i]));
                chartControl1.Series[1].Points.Add(new DevExpress.XtraCharts.SeriesPoint(i + 1, g[i]));
                chartControl1.Series[2].Points.Add(new DevExpress.XtraCharts.SeriesPoint(i + 1, b[i]));
            }
        }
    }
}