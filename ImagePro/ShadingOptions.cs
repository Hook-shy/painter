﻿using DevExpress.Utils.Layout;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImagePro
{
    public partial class ShadingOptions : DevExpress.XtraEditors.XtraUserControl
    {
        public ShadingOptions(Dictionary<Color, Color> cc)
        {
            InitializeComponent();
            ColorDic = cc;
            List<Color> colorKeys = ColorDic.Keys.ToList();
            colors = colorKeys.Select(ck => new Color[] { ck, cc[ck] }).ToArray();
            UpdateColors();
            windowsUIButtonPanel1.ButtonClick += (s, e) =>
            {
                if (OnStart != null && ColorDic.Keys.Count > 1)
                {
                    OnStart(ColorDic);
                }
            };
        }

        public delegate void Start(Dictionary<Color, Color> cc);
        public event Start OnStart;
        public Dictionary<Color, Color> ColorDic { get; set; }
        private Color[][] colors;

        public void ChangeColors(int row, int col, Color color)
        {
            Dictionary<Color, Color> cc = new Dictionary<Color, Color>();
            colors[row][col] = color;
            for (int i = 0; i < colors.GetLength(0); i++)
            {
                cc.Add(colors[i][0], colors[i][1]);
            }
            ColorDic = cc;
        }

        public void UpdateColors(Dictionary<Color, Color> cc)
        {
            ColorDic = cc;
            List<Color> colorKeys = ColorDic.Keys.ToList();
            colors = colorKeys.Select(ck => new Color[] { ck, cc[ck] }).ToArray();
            UpdateColors();
        }

        private void UpdateColors()
        {
            tablePanel1.Controls.Clear();

            tablePanel1.Rows.Clear();
            tablePanel1.Rows.Add(new TablePanelRow(TablePanelEntityStyle.Absolute, 26F));
            tablePanel1.Rows.Add(new TablePanelRow(TablePanelEntityStyle.Absolute, 26F));
            tablePanel1.Rows.AddRange(colors.Select(cc => new TablePanelRow(TablePanelEntityStyle.Absolute, 26F)).ToArray());

            string[] heads = new string[] { "序号", "中心色", "目标色" };
            for (int i = 0; i < heads.Length; i++)
            {
                LabelControl lab = new LabelControl();
                lab.Text = heads[i];
                tablePanel1.Controls.Add(lab);
                tablePanel1.SetColumn(lab, i);
                tablePanel1.SetRow(lab, 0);
            }

            for (int i = 0; i < colors.GetLength(0); i++)
            {
                LabelControl lab = new LabelControl();
                lab.Text = $"{i + 1}";
                tablePanel1.Controls.Add(lab);
                tablePanel1.SetColumn(lab, 0);
                tablePanel1.SetRow(lab, i + 1);

                ColorPickEdit cpe1 = new ColorPickEdit();
                cpe1.EditValue = colors[i][0];
                //cpe1.Properties.Buttons.AddRange(new EditorButton[] { new EditorButton(ButtonPredefines.Combo) });
                cpe1.Properties.ShowWebSafeColors = true;
                tablePanel1.Controls.Add(cpe1);
                tablePanel1.SetColumn(cpe1, 1);
                tablePanel1.SetRow(cpe1, i + 1);
                int m = i;
                cpe1.EditValueChanged += (s, e) =>
                {
                    ChangeColors(m, 0, (Color)cpe1.EditValue);
                };

                ColorPickEdit cpe2 = new ColorPickEdit();
                cpe2.EditValue = colors[i][1];
                //cpe2.Properties.Buttons.AddRange(new EditorButton[] { new EditorButton(ButtonPredefines.Combo) });
                cpe2.Properties.ShowWebSafeColors = true;
                tablePanel1.Controls.Add(cpe2);
                tablePanel1.SetColumn(cpe2, 2);
                tablePanel1.SetRow(cpe2, i + 1);
                int n = i;
                cpe2.EditValueChanged += (s, e) =>
                {
                    ChangeColors(n, 1, (Color)cpe2.EditValue);
                };
            }
        }
    }
}
