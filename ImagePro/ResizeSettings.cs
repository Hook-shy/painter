﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImagePro
{
    public partial class ResizeSettings : DevExpress.XtraEditors.XtraUserControl
    {
        public ResizeSettings()
        {
            InitializeComponent();
            spinEditWidth.EditValueChanged += (s, e) =>
            {
                SWidth = Convert.ToInt32(spinEditWidth.EditValue.ToString());
            };
            spinEditHeight.EditValueChanged += (s, e) =>
            {
                SHeight = Convert.ToInt32(spinEditHeight.EditValue.ToString());
            };
            toggleSwitchPerCent.EditValueChanged += (s, e) =>
            {
                Percent = Convert.ToBoolean(toggleSwitchPerCent.EditValue.ToString());
            };
            radioGroupMode.EditValueChanged += (s, e) =>
            {
                ResizeMode = ((ResizeMode)Enum.Parse(typeof(ResizeMode), radioGroupMode.EditValue.ToString()));
            };
            windowsUIButtonPanel1.ButtonClick += (s, e) =>
            {
                if(OnStart != null)
                {
                    OnStart();
                }
            };
        }

        public delegate void Start();
        public event Start OnStart;
        public int SWidth { get; set; } = 1600;
        public int SHeight { get; set; } = 900;
        public bool Percent { get; set; } = false;
        public ResizeMode ResizeMode { get; set; } = ResizeMode.Fill;
    }
}
