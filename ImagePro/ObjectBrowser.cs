﻿using DevExpress.XtraEditors;
using DevExpress.XtraNavBar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImagePro
{
    public partial class ObjectBrowser : DevExpress.XtraEditors.XtraUserControl
    {
        public ObjectBrowser(List<ImageItem> imageItems, List<ClusteringItem> clusteringItems)
        {
            InitializeComponent();
            ImageItems = imageItems;
            ClusteringItems = clusteringItems;
            UpdateObject();
        }

        public List<ImageItem> ImageItems { get; set; }
        public List<ClusteringItem> ClusteringItems { get; set; }
        public delegate void ImageItemClick(ImageItem imageItem);
        public delegate void ClusterimgItemClick(ClusteringItem clusteringItem);
        public ImageItemClick OnImageItemLinkClick;
        public ClusterimgItemClick OnClusterimgItemLinkClick;

        public void UpdateObject()
        {
            navBarGroupImgs.ItemLinks.Clear();
            foreach (var item in ImageItems)
            {
                NavBarItem navBarItem = new NavBarItem();
                navBarItem.Caption = item.Name;
                navBarItem.ImageOptions.SmallImage = item.Image;
                navBarItem.ImageOptions.LargeImage = item.Image;
                navBarItem.ImageOptions.SmallImageSize = new Size(80, 45);
                navBarItem.ImageOptions.LargeImageSize = new Size(160, 90);
                navBarGroupImgs.ItemLinks.Add(new NavBarItemLink(navBarItem));
                navBarItem.LinkClicked += (s, e) => { OnImageItemLinkClick(item); };
            }
            navBarGroupClasses.ItemLinks.Clear();
            foreach (var item in ClusteringItems)
            {
                NavBarItem navBarItem = new NavBarItem();
                navBarItem.Caption = item.Name;
                navBarItem.ImageOptions.SmallImage = item.PreviewBitmap;
                navBarItem.ImageOptions.LargeImage = item.PreviewBitmap;
                navBarItem.ImageOptions.SmallImageSize = new Size(80, 45);
                navBarItem.ImageOptions.LargeImageSize = new Size(160, 90);
                navBarGroupClasses.ItemLinks.Add(new NavBarItemLink(navBarItem));
                navBarItem.LinkClicked += (s, e) => { OnClusterimgItemLinkClick(item); };
            }
        }

        public void ToClickItemLink(Object item)
        {
            if (item is ImageItem)
            {
                OnImageItemLinkClick((ImageItem)item);

            }
            else if (item is ClusteringItem)
            {
                OnClusterimgItemLinkClick((ClusteringItem)item);
            }
        }
    }
}
