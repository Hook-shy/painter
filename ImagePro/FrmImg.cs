﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImagePro
{
    public partial class FrmImg : DevExpress.XtraEditors.XtraForm
    {
        public FrmImg(ImageItem imageItem)
        {
            InitializeComponent();
            ImageItem = imageItem;
            Text = imageItem.Name;
            pictureBox1.Image = imageItem.Image;
        }

        public ImageItem ImageItem { get; }
    }
}