﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ImagePro
{
    public class ClusteringItem
    {
        public ClusteringItem(ImageItem imageItem, int category, int limitTimes, int skip)
        {
            List<Color> colors = new List<Color>();
            for (int i = 0; i < imageItem.Image.Height; i += skip)
            {
                for (int j = 0; j < imageItem.Image.Width; j += skip)
                {
                    colors.Add(((Bitmap)imageItem.Image).GetPixel(j, i));
                }
            }
            ImageItem = imageItem;
            ColorClustering = new ColorClustering(colors, category, limitTimes);
            Skip = skip;
            Name = $"{imageItem.Name}(聚类:{category}_{limitTimes}_{skip})";
        }

        public void Start(ColorClustering.Progress por)
        {
            ColorClustering.Start(por);
            Bitmap bg = new Bitmap(160, 90);
            using (Graphics g = Graphics.FromImage(bg))
            {
                g.DrawImage(ImageItem.Image, 0, 0, bg.Width - 30, bg.Height);
                int count = ColorClustering.ColorClasses.Count;
                for (int i = 0; i < ColorClustering.ColorClasses.Count; i++)
                {
                    g.FillRectangle(new SolidBrush(ColorClustering.ColorClasses[i].Center), new Rectangle(bg.Width - 30, i * bg.Height / count, 30, (i + 1) * bg.Height / count));
                    g.DrawRectangle(new Pen(Color.White), new Rectangle(bg.Width - 30, i * bg.Height / count, 30, (i + 1) * bg.Height / count));
                }
            }
            PreviewBitmap = bg;
        }

        public Bitmap PreviewBitmap { get; set; }
        public string Name { get; set; }
        public int Skip { get; }
        public ImageItem ImageItem { get; set; }
        public ColorClustering ColorClustering { get; set; }
    }
}
