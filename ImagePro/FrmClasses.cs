﻿using DevExpress.XtraCharts;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImagePro
{
    public partial class FrmClasses : DevExpress.XtraEditors.XtraForm
    {
        public FrmClasses(ClusteringItem clusteringItem)
        {
            InitializeComponent();
            ClusteringItem = clusteringItem;
            ImageItem = clusteringItem.ImageItem;
            ColorClustering = clusteringItem.ColorClustering;
            Text = clusteringItem.Name;
            DrawChart();
        }

        //public FrmClasses(ImageItem imageItem, ColorClustering.Progress curProgress, int category = 5, int limitTimes = 100, int skip = 5)
        //{
        //    InitializeComponent();
        //    ImageItem = imageItem;
        //    CurProgress = curProgress;
        //    Text = $"{imageItem.Name}_聚类_{category}_{limitTimes}_{skip}";
        //    List<Color> colors = new List<Color>();
        //    for (int i = 0; i < ImageItem.Image.Height; i += skip)
        //    {
        //        for (int j = 0; j < ImageItem.Image.Width; j += skip)
        //        {
        //            colors.Add(((Bitmap)ImageItem.Image).GetPixel(j, i));
        //        }
        //    }
        //    ColorClustering = new ColorClustering(colors, category, limitTimes);
        //    ColorClustering.Start(CurProgress);
        //    DrawChart();
        //}

        //public ColorClustering.Progress CurProgress { get; }
        public ImageItem ImageItem { get; }
        public ColorClustering ColorClustering { get; }
        public ClusteringItem ClusteringItem { get; }

        private void DrawChart()
        {
            chartControl1.Series[0].Points.Clear();
            chartControl1.Series[0].Points.AddRange(ColorClustering.ColorClasses.OrderByDescending(c => c.Members.Count).Select(c =>
              {
                  SeriesPoint point = new SeriesPoint($"#{c.Center.R.ToString("X")}{c.Center.G.ToString("X")}{c.Center.B.ToString("X")}", c.Members.Count);
                  point.Color = c.Center;
                  return point;
              }).ToArray());

            chartControl2.Series.Clear();
            chartControl2.Series.AddRange(ColorClustering.ColorClasses.Select(c =>
           {
               Series series = new Series($"#{c.Center.R.ToString("X")}{c.Center.G.ToString("X")}{c.Center.B.ToString("X")}", ViewType.Bubble);
               series.Points.AddRange(c.Members.Select(m =>
               {
                   SeriesPoint point = new SeriesPoint(m.R, m.G, m.B);
                   point.Color = c.Center;
                   return point;
               }).ToArray());
               return series;
           }).ToArray());
            foreach (Series serie in chartControl2.Series)
            {
                ((BubbleSeriesView)serie.View).AutoSize = false;
                ((BubbleSeriesView)serie.View).MaxSize = 3;
            }
            Palette palette = new Palette("Custom Palette");
            foreach (var item in ColorClustering.ColorClasses)
            {
                palette.Add(item.Center);
            }
            chartControl2.PaletteRepository.RegisterPalette(palette);
            chartControl2.PaletteName = "Custom Palette";
        }
    }
}